Feature: Verifying the page url goes to the right place and status cod 200 is returned


  # website can be etherscan or polygonscan
  @HC
  Scenario: Checking if the website is functioning
    Given the HealthCheck for "etherscan" is triggered
    When the status response is returned
    Then the status response will return code 200 OK


  Scenario: The Web home page should have correct url

    Given I go to the site "etherscan"
    Then current url should be "etherscan"

