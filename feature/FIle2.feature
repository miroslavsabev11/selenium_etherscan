Feature: Verifying the wallet address

  # for now the only wallet addresses available are Vitalik and Mark
  # sites are etherscan and polygonscan
  #'ERC-20 Token Txns' for Polygon and 'Erc20 Token Txns' for EtherScan
  Scenario: Verify the wallet address of the person being checked

    Given I go to the site "etherscan"
    Then the wallet address of "Vitalik" should be visible
    And the "Erc20 Token Txns" bar should be visible


  Scenario: Check the latest 25 transactions and extract the "OUT" types
    Given webpage is open
    When the latest transactions have been read/collected
    And if any "OUT" transactions are detected
    Then new excel sheet will be generated with the information