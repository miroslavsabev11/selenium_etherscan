import logging
import sys
import time
from behave import given, when, then, step
import requests
from feature.CommonConfigs import urlconfig, env, locators
from feature.CommonConfigs.locators import LOCATORS
from feature.CommonFunc import webcommon


@given('the HealthCheck for "{environment}" is triggered')
def healtheck_trigger(context, environment):
    try:
        if 'etherscan' in environment:
            uri = urlconfig.URLCONFIG.get('etherscan')
            logging.info(f"Running Health Check on {environment}.")
        elif 'polygonscan' in environment:
            uri = urlconfig.URLCONFIG.get('polygonscan')
            logging.info(f"Running Health Check for {environment}")

        headers = urlconfig.URLCONFIG.get('headers')
        response = requests.get(uri, headers=headers)
        context.status_response = response.status_code
    except:
        logging.error(f"Status code: {context.status_response}")
        logging.error(f"Error message: {sys.exc_info()}")
        logging.error("Failed to send the GET request!")
        assert False
    time.sleep(2)


@when('the status response is returned')
def response_returned(context):
    logging.info("Returning status response")


@then('the status response will return code 200 OK')
def check_status_response(context):
    if context.status_response == 200:
        logging.info('Status response is 200, Test passed!')
        assert True
    else:
        logging.error("Health Check failed, status code is NOT 200!")
        assert False


# ===================================================================
@step("I go to '{page}' page")
@given('I go to the site "{page}"')
def go_to_page(context, page):
    """
    Step definition to go to the specified url.
    :param context:
    :param url:
    """
    page = urlconfig.URLCONFIG.get(page)
    print("Navigating to the page: {}".format(page))

    webcommon.go_to(context, page)


@then('current url should be "{expected_url}"')
def verify_current_url(context, expected_url):
    """
    Verifies the current uls is as expected_url
    :param context:
    :param expected_url:
    """
    if expected_url == 'etherscan':
        expected_url = urlconfig.URLCONFIG['etherscan']
    if expected_url == 'polygon':
        expected_url = urlconfig.URLCONFIG['polygonscan']

    webcommon.assert_current_url(context, expected_url)


@then('the wallet address of "{person}" should be visible')
def check_wallet_address(context, person):
    if person == 'Vitalik':
        expected_text = env.get_wallet(person)
    elif person == 'Mark':
        expected_text = env.get_wallet(person)

    header_locator = LOCATORS['main_wallet_address']
    contains = webcommon.element_contains_text(context, expected_text, header_locator['type'],
                                               header_locator['locator'])
    assert contains, f'Wallet address on the page does not match the expected wallet of {person}. Wallet: {expected_text} '


@then('the "{erc20_token_bar}" bar should be visible')
def erc_20_token_bar_check(context, erc20_token_bar):
    # import pdb; pdb.set_trace()
    expected_bars = ['ERC-20 Token Txns', 'Erc20 Token Txns']
    if erc20_token_bar not in expected_bars:
        raise Exception("The passed in nav_bar type is not one of expected."
                        "Actual: {}, Expected in: {}".format(erc20_token_bar, expected_bars))

    locator_info = locators.LOCATORS.get('erc20_token_bar')
    locator_type = locator_info['type']
    locator_text = locator_info['locator']

    nav_element = webcommon.find_element(context, locator_type, locator_text)

    webcommon.assert_element_visible(nav_element)


# TODO
@given(u'webpage is open')
def step_impl(context):
    pass


@when('the latest transactions have been read/collected')
def step_impl(context):
    pass


@when(u'if any "OUT" transactions are detected')
def step_impl(context):
    pass


@then(u'new excel sheet will be generated with the information')
def step_impl(context):
    pass
