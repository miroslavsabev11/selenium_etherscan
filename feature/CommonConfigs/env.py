import lxml.etree as ET


def get_wallet(argument):
    switcher = {
        "Vitalik": "0xAb5801a7D398351b8bE11C439e05C5B3259aeC9B",
        "Mark": "0x293Ed38530005620e4B28600f196a97E1125dAAc"
    }
    return switcher.get(argument, "n")