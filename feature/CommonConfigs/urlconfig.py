URLCONFIG = {

    'etherscan': 'https://etherscan.io/address/0xab5801a7d398351b8be11c439e05c5b3259aec9b#tokentxns',
    'polygonscan': 'https://polygonscan.com/address/0x293ed38530005620e4b28600f196a97e1125daac#tokentxns',
    'home': '/',
    'base_url': 'etherscan.io',
    'TopStats': '/topstat',
    # 'etherscan': 'https://etherscan.io',
    # 'polygonscan': 'https://polygonscan.com',
    'headers': {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

}
